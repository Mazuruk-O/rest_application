package com.houses.service;

import com.houses.domain.HouseImpl;

import java.util.List;

public interface HouseService {

    List<HouseImpl> findAllHouse();

    HouseImpl findByAddress(String address);

    HouseImpl create(HouseImpl house);

    HouseImpl saveChanges(String id, HouseImpl  updateHouse);

    HouseImpl deleteHouse(String address);
}
