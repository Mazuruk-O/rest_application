package com.houses.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("owner-service")
public interface FeignController {
    @RequestMapping("/info")
    String info();
}
