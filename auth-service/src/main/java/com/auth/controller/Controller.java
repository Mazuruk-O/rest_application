package com.auth.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping("/")
    public String start(){
        return "hello. Not auth.";
    }

    @GetMapping("/data")
    public String data(){
        return "Data. You are auth=)";
    }

    @GetMapping("/true_auth")
    public String true_auth(){
        return "true_auth";
    }
}
