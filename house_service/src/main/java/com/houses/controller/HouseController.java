package com.houses.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.houses.domain.House;
import com.houses.domain.HouseImpl;
import com.houses.domain.Owner;
import com.houses.domain.StatusHouse;
import com.houses.service.HouseService;
import com.houses.service.HouseServiceImpl;
import com.houses.service.OwnerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class HouseController {
    @Autowired
    private HouseServiceImpl houseService;

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private FeignController feignController;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/")
    public ResponseEntity<?> startController(){
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body("@GetMapping(\"/houses\")\n" +
                        "@PostMapping(\"/houses\")\n" +
                        "@PutMapping(\"/houses\")\n" +
                        "@DeleteMapping(\"/houses/{address}\")");
    }

    //@PreAuthorize("#oauth2.hasScope('server') or #name.equals('demo')")
    @GetMapping("/houses")
    public ResponseEntity<?> allHouse(){
        log.info(" @GetMapping(\"/houses\")");

        return ResponseEntity.status(HttpStatus.OK).body(houseService.findAllHouse());
    }

    @PostMapping("/houses")
    public ResponseEntity<?> newHouse(@RequestBody HouseImpl house){
        log.info("@PostMapping(\"/houses\"), address = " + house.getAddress());

        return ResponseEntity.status(HttpStatus.OK).body(houseService.create(house));
    }

    @PutMapping("/houses")
    public ResponseEntity<?> replaceHouse(@RequestBody HouseImpl house) {
        log.info("@PutMapping(\"/houses/{address}\") = " + house.getAddress());

        HouseImpl forReplace = houseService.findByAddress(house.getAddress());

        return ResponseEntity.status(HttpStatus.OK).body(houseService.saveChanges(forReplace.getId(), house));
    }

    @DeleteMapping("/houses/{address}")
    public ResponseEntity<?> deleteEmployee(@PathVariable String address) {
        return ResponseEntity.status(HttpStatus.OK).body(houseService.deleteHouse(address));
    }

    @GetMapping("/info")
    public String info() {
        return feignController.info();
    }
}