package com.houses.service;

import com.houses.domain.Owner;
import com.houses.repository.OwnerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OwnerService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private OwnerRepository ownerRepository;

    public List<Owner> findAllOwner(){
        return ownerRepository.findAll();
    }

    public Owner create(Owner owner){
        return ownerRepository.insert(owner);
    }

    public Owner saveChanges(Owner owner){
        return ownerRepository.save(owner);
    }

    public void deleteOwner(Owner owner){
        ownerRepository.delete(owner);
    }
}
