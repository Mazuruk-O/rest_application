package com.houses;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients
@EnableWebMvc

public class HouseServiceMain {
    public static void main(String[] args) {
        SpringApplication.run(HouseServiceMain.class, args);
    }
}