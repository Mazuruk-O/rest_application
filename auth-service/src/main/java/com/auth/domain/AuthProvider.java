package com.auth.domain;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}
