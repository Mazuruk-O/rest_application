package com.houses.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;
import java.util.Objects;

@Data
@Document("houses")
public class HouseImpl implements House, Serializable {
    @Id
    private String id;

    private String address;
    private int numberEntrances;
    private int numberApartments;
    private int numberFloors;
    private int yearConstruction;
    private StatusHouse statusHouse;
}
