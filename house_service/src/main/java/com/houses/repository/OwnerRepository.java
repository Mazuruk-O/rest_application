package com.houses.repository;


import com.houses.domain.HouseImpl;
import com.houses.domain.Owner;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

public interface OwnerRepository extends MongoRepository<Owner, String> {
    Owner findOwnerByOwnerHouses(HouseImpl house);
}
