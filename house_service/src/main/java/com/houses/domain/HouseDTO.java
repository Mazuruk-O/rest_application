package com.houses.domain;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
import java.util.Objects;

@Data
public class HouseDTO {
    private MultipartFile imageFile;
    private HouseImpl houseData;
}
