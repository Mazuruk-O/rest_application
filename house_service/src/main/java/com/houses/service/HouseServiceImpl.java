package com.houses.service;

import com.houses.domain.HouseImpl;
import com.houses.domain.StatusHouse;
import com.houses.repository.HouseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class HouseServiceImpl implements HouseService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private HouseRepository houseRepository;


    @Override
    public List<HouseImpl> findAllHouse() {
        List<HouseImpl> houses = houseRepository.findAll();

        Assert.notEmpty(houses,"Error in HouseServiceImpl.class, findAllHouse() is empty");

        log.info("Get all houses!");

        return houses;
    }

    @Override
    public HouseImpl findByAddress(String address) {
        Assert.hasLength(address,"Error in HouseServiceImpl.class, findByAddress(String address) [address] is empty");

        HouseImpl house = houseRepository.findByAddress(address);

        log.info("Find by Address: " + address);

        return house;
    }

    @Override
    public HouseImpl create(HouseImpl house) {
        Assert.notNull(house, "Input house is null. Can't create new house, address = " + house.getAddress());

        HouseImpl existing = houseRepository.findByAddress(house.getAddress());

        Assert.isNull(existing, "House already exists: " + house.getAddress());

        house.setStatusHouse(StatusHouse.ACTIVE);

        HouseImpl houseSave = houseRepository.insert(house);

        log.info("New house has been created: " + houseSave.getAddress());

        return houseSave;
    }

    @Override
    public HouseImpl saveChanges(String id, HouseImpl updateHouse) {
        Assert.hasLength(id,"id Length = 0");

        Optional<HouseImpl> existing = houseRepository.findById(id);

        Assert.notNull(existing.get() , "Can't find house with address " + updateHouse.getAddress());

        Assert.notNull(updateHouse, "Can't find house with address " + updateHouse.getAddress());

        updateHouse.setId(id);

        log.debug("House {} changes has been saved", updateHouse.getAddress());

        return houseRepository.save(updateHouse);
    }

    @Override
    public HouseImpl deleteHouse(String address) {
        Assert.hasLength(address,"address Length = 0");

        HouseImpl existing = houseRepository.findByAddress(address);

        Assert.notNull(existing, "Can't find house with address " + address);

        existing.setStatusHouse(StatusHouse.DELETE);

        return houseRepository.save(existing);
    }
}
