package com.houses.domain;

public enum StatusHouse {
    ACTIVE("ACTIVE"),DELETE("DELETE"),FREEZE("FREEZE");

    String status;

    StatusHouse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
