package com.houses.repository;

import com.houses.domain.HouseImpl;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HouseRepository extends MongoRepository<HouseImpl, String> {
    HouseImpl findByAddress(String address);
}
