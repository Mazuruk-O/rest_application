package com.houses.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@Data
@Document("users")
public class Owner implements Serializable {
    @Id
    private String id;

    private String username;
    private String firstname;
    private String lastname;
    private String surname;
    private String email;
    private List<HouseImpl> ownerHouses;

}
